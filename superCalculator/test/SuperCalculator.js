let homepage = require("../pages/home");
describe("demo app test", function () {

    it("Addition test", function () {
        homepage.getUrl("http://juliemr.github.io/protractor-demo/");

        homepage.takeFirstNumber(102);
        homepage.takeSecondNumber(401);

        homepage.additionOperator();

        homepage.clickButton();

        homepage.verifyResult("503");

        browser.sleep(2000);
    }),
        it("Subtraction test", function () {
            homepage.getUrl("http://juliemr.github.io/protractor-demo/");

            homepage.takeFirstNumber(309);
            homepage.takeSecondNumber(112);

            homepage.subtractionOperation();

            homepage.clickButton();

            homepage.verifyResult("197");

            browser.sleep(2000);
        }),
        it("Division test", function () {
            homepage.getUrl("http://juliemr.github.io/protractor-demo/");

            homepage.takeFirstNumber(102);
            homepage.takeSecondNumber(2);

            homepage.divisionOperation();

            homepage.clickButton();

            homepage.verifyResult("51");

            browser.sleep(2000);
        }),
        it("Multiplication test", function () {
            homepage.getUrl("http://juliemr.github.io/protractor-demo/");

            homepage.takeFirstNumber(5);
            homepage.takeSecondNumber(9);

            homepage.multiplicationOperation();

            homepage.clickButton();

            homepage.verifyResult("45");

            browser.sleep(2000);
        }),
        it("Modulo test", function () {
            homepage.getUrl("http://juliemr.github.io/protractor-demo/");

            homepage.takeFirstNumber(10);
            homepage.takeSecondNumber(3);

            homepage.moduloOperation();

            homepage.clickButton();

            homepage.verifyResult("1");

            browser.sleep(2000);
        })
});