let homepage = function () {
    let firstNumber = element(By.model("first"));
    let secondNumber = element(By.model("second"));
    let clickB = element(By.css('[ng-click="doAddition()"]'));

    let addition = element(by.css("[ng-model='operator']"))
    let subtraction = element(by.css("[value='SUBTRACTION']"));
    let division = element(by.css("[value='DIVISION']"))
    let multiplication = element(by.css("[value='MULTIPLICATION']"));
    let modulo = element(by.css("[value='MODULO']"));

    this.getUrl = function (url) {
        browser.get(url)
    };
    
    this.takeFirstNumber = function (first) {
        firstNumber.sendKeys(first);
    }
    this.takeSecondNumber = function (second) {
        secondNumber.sendKeys(second);
    }
    this.additionOperator = function () {
        addition.click();
    }
    this.subtractionOperation = function () {
        subtraction.click();
    }
    this.divisionOperation = function () {
        division.click();
    }
    this.multiplicationOperation = function () {
        multiplication.click();
    }
    this.moduloOperation = function () {
        modulo.click();
    }

    this.clickButton = function () {
        clickB.click();
    }

    this.verifyResult = function (expectedNumber) {
        let result = element(By.cssContainingText(".ng-binding", expectedNumber));
        expect(expectedNumber).toEqual(result.getText());
    }

};

module.exports = new homepage;